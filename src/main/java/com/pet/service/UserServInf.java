package com.pet.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.pet.model.Pets;
import com.pet.model.User;

public interface UserServInf {

	void saveUser(User user);

	List<Pets> home();

	List<Pets> petList(int id);

	void savepet(int id,Pets pets);

	int checkUser(String name, String passwd);

}
