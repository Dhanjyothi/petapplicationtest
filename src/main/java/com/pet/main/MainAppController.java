package com.pet.main;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.pet.model.Pets;
import com.pet.model.User;
import com.pet.service.UserServInf;

@Controller
public class MainAppController {
	@Autowired
	UserServInf ser;

	@RequestMapping("/Login")
	public ModelAndView login(@ModelAttribute("user") User user) {

		return new ModelAndView("LoginPage");
	}

	@RequestMapping("/checkUser")
	public ModelAndView checkUser(HttpServletRequest req, @ModelAttribute("user") User user) {
		String str = " ";
		String name = req.getParameter("user_name");
		String passwd = req.getParameter("user_passwd");
		int id = ser.checkUser(name, passwd);

		HttpSession session = req.getSession();
		session.setAttribute("id", id);

		if (id != 0) {

			str = "redirect:/home";

		} else {
			str = "InvalidUser";
		}
		return new ModelAndView(str);
	}

	@RequestMapping("/Register")
	public ModelAndView register(@ModelAttribute("user") User user) {
		return new ModelAndView("Registration");
	}

	@RequestMapping("/save")
	public ModelAndView saveUser(@ModelAttribute("user") User user) {
		ser.saveUser(user);
		return new ModelAndView("redirect:/Login");
	}
	@RequestMapping("/mypet")
	public ModelAndView mypet() {
		
		return new ModelAndView("redirect:/petlist");
	}

	@RequestMapping("/home")
	public ModelAndView home( @ModelAttribute("user") Pets pets) {

		List<Pets> ls = ser.home();
		return new ModelAndView("HomePage", "list", ls);
	}

	@RequestMapping("/addPet")
	public ModelAndView addpet(@ModelAttribute("pets") Pets pets) {

		return new ModelAndView("AddPet");
	}

	@RequestMapping("/savePet")
	public ModelAndView savepet(HttpServletRequest req, @ModelAttribute("pets") Pets pets) {
		HttpSession session=req.getSession();
		int id=(int) session.getAttribute("id");
		ser.savepet(id, pets);

		return new ModelAndView("redirect:/petlist");
	}

	@RequestMapping("/petlist")
	public ModelAndView petList(HttpServletRequest req, @ModelAttribute("pets") Pets pets) {
		HttpSession session=req.getSession();
		int id = (int) session.getAttribute("id");
		List<Pets> ls = ser.petList(id);
		return new ModelAndView("Mypet", "list", ls);
	}
	@RequestMapping("/logout")
	public ModelAndView logout(@ModelAttribute("user") User user) {
	
	

		return new ModelAndView("LoginPage");
	}


}
