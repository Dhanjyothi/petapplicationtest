package com.pet.test;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages="com.pet")
public class TestBeanConfig {

}
